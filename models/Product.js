// Require mongoose
const mongoose = require("mongoose");

// Creating a schema design
const productSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: [true, "Product name is required"]
		},
		description: {
			type: String,
			required: [true, "Product description is required"]
		},
		price: {
			type: Number,
			required: [true, "Product price is required"]
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date
		},
		orders: [
			{
				userId: {
					type: String,
					required: [true, "User Id is required"]
				},
				orderedOn: {
					type: Date,
					default: new Date
				}
			}
		]
	}
)

// Export to another file
module.exports = mongoose.model("Product", productSchema);